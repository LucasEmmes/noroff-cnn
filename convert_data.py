import cv2 as cv
import numpy as np

img = cv.imread("data.png")

# Jeg har 20 bokstaver per rekke, og 24 rekker totalt
cells = [np.hsplit(row, 20) for row in np.vsplit(img, 24)]

# Jeg har 4 rekker for hver bokstav
data = "".join([i*4 for i in "ABCDEF"])
for n, row in enumerate(cells):
    section = data[n]
    for i, image in enumerate(row):
        cv.imwrite(f"dataset/{section}/{i+n*20}.png", np.array(image))

print("Ferdig")