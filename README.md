# Noroff CNN  
In this project I utilized tensorflow to make a CNN which  
can read my handwriting.

## Prerequisites
To use this script you will need:
- matplotlib
- tensorflow

## Extras
Additionally, I have provided a script to cut up the dataset-image from the previous projects.

## Results
![](matplotlib_graph.png)